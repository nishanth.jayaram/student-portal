import React from "react";
import { Outlet } from "react-router-dom";
import Header from "./Header/Header";
import { Fragment } from "react";

const Layout = () => {
  return (
    <Fragment>
      <Header></Header>
      <section style={{ paddingTop: "1.5rem" }}>
        <Outlet />
      </section>
    </Fragment>
  );
};

export default Layout;
