import React, { useState } from "react";
import classes from "./Header.module.css";
import AddEditStudents from "../../components/Students/AddEditStudents";

const Header = () => {
  const [showModal, setShowModal] = useState(false);

  const showHideModal = () => {
    setShowModal(showModal ? false : true);
  };

  return (
    <>
      <header className={classes.header__container}>
        <div className={classes.header__left}>Student Portal</div>
        <div className={classes.header__right}>
          <button
            className={classes.add__btn}
            onClick={() => setShowModal(showModal ? false : true)}
          >
            Add Student
          </button>
        </div>
      </header>
      {showModal && (
        <AddEditStudents showModal={showModal} showHideModal={showHideModal} />
      )}
    </>
  );
};

export default Header;
