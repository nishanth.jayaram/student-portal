import { configureStore } from "@reduxjs/toolkit";
import studentReducer from "./students";
import uiReducer from "./ui";

const store = configureStore({
  reducer: { student: studentReducer, ui: uiReducer },
});

export default store;
