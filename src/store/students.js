import { createSlice } from "@reduxjs/toolkit";

const initialStudentState = { data: [] };

const studentSlice = createSlice({
  name: "student",
  initialState: initialStudentState,
  reducers: {
    set(state, action) {
      state.data = [...action.payload];
    },
    add(state, action) {
      state.data = [action.payload, ...state.data];
    },
    edit(state, action) {
      const changedItem = action.payload;
      const existingItem = state.data.find(
        (item) => item._id === changedItem._id
      );
      if (existingItem) {
        existingItem.first_name = changedItem.first_name;
        existingItem.last_name = changedItem.last_name;
        existingItem.age = changedItem.age;
        existingItem.gender = changedItem.gender;
        existingItem.class = changedItem.class;
        existingItem.address = changedItem.address;
      }
    },
    delete(state, action) {
      state.data = state.data.filter(
        (eachData) => eachData._id !== action.payload
      );
    },
  },
});

export const studentActions = studentSlice.actions;

export default studentSlice.reducer;
