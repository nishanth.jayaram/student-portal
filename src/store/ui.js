import { createSlice } from "@reduxjs/toolkit";

const initialUIState = {
  showLoader: false,
  toasters: [],
};

const uiSlice = createSlice({
  name: "ui",
  initialState: initialUIState,
  reducers: {
    showHideLoader(state, action) {
      state.showLoader = action.payload;
    },
    addToaster(state, action) {
      const newToaster = {
        id: new Date().getTime(),
        type: action.payload.type,
        message: action.payload.message,
      };
      state.toasters.push(newToaster);
    },
    removeToaster(state, action) {
      state.toasters = state.toasters.filter(
        (eachToaster) => eachToaster.id !== action.payload.id
      );
    },
  },
});

export const uiActions = uiSlice.actions;

export default uiSlice.reducer;
