import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Table from "../components/Table/Table";
import { studentActions } from "../store/students";
import AddEditStudents from "../components/Students/AddEditStudents";
import Dialog from "../components/Dialog/Dialog";
import axiosInstance from "../services/axiosInstance";
import { uiActions } from "../store/ui";

let studentDtl = {};
const headers = [
  "id",
  "first_name",
  "last_name",
  "age",
  "gender",
  "class",
  "address",
];

const Home = () => {
  const dispatch = useDispatch();
  const [showEditModal, setShowEditModal] = useState(false);
  const studentsData = useSelector((state) => state?.student?.data);
  const [showConfirmDialog, setShowConfirmDialog] = useState(false);

  useEffect(() => {
    const fetchStudents = async () => {
      try {
        dispatch(uiActions.showHideLoader(true));
        const allStudents = await axiosInstance.get("/student");
        dispatch(studentActions.set(allStudents.data));
        dispatch(uiActions.showHideLoader(false));
      } catch (error) {
        dispatch(uiActions.showHideLoader(false));
        dispatch(
          uiActions.addToaster({
            type: "error",
            message: error?.message || "Something went wrong",
          })
        );
      }
    };
    fetchStudents();
  }, []);

  const showHideModal = () => {
    studentDtl = {};
    setShowEditModal(showEditModal ? false : true);
  };

  const editStudentHandler = (selectedStudent) => {
    setShowEditModal(true);
    studentDtl = selectedStudent;
  };

  const deleteStudentHandler = async () => {
    try {
      dispatch(uiActions.showHideLoader(true));
      await axiosInstance.delete(`/student/${studentDtl._id}`);
      dispatch(studentActions.delete(studentDtl._id));
      setShowConfirmDialog(false);
      dispatch(uiActions.showHideLoader(false));
      dispatch(
        uiActions.addToaster({
          type: "success",
          message: `${studentDtl.first_name} ${studentDtl.last_name} deleted successfully!`,
        })
      );
      studentDtl = {};
    } catch (error) {
      dispatch(uiActions.showHideLoader(false));
      dispatch(
        uiActions.addToaster({
          type: "error",
          message: error?.message || "Something went wrong",
        })
      );
    }
  };
  return (
    <>
      <div style={{ padding: "0 1.5rem" }}>
        <Table
          tableHeaders={headers}
          tableData={studentsData}
          onEdit={editStudentHandler}
          onDelete={(data) => {
            studentDtl = data;
            setShowConfirmDialog(true);
          }}
        ></Table>
      </div>
      {showEditModal && (
        <AddEditStudents
          showModal={showEditModal}
          studentDtl={studentDtl}
          showHideModal={showHideModal}
        />
      )}
      {showConfirmDialog && (
        <Dialog
          showModal={showConfirmDialog}
          isConfirmation={true}
          header="Confirmation"
          message={`Are you sure? do you want to delete ${studentDtl.first_name} ${studentDtl.last_name}?`}
          onClose={() => {
            studentDtl = {};
            setShowConfirmDialog(false);
          }}
          onConfirm={deleteStudentHandler}
        />
      )}
    </>
  );
};

export default Home;
