import ReactDOM from "react-dom";
import classes from "./Modal.module.css";

const Modal = ({ isOpen, children }) => {
  if (!isOpen) return null;

  return ReactDOM.createPortal(
    <div className={classes.modal__backdrop}>
      <div className={classes.modal__content}>{children}</div>
    </div>,
    document.getElementById("overlays")
  );
};

export default Modal;
