import React from "react";
import classes from "./Toaster.module.css";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { uiActions } from "../../store/ui";

const Toaster = () => {
  const toasters = useSelector((state) => state?.ui?.toasters) || [];
  const dispatch = useDispatch();
  const timeoutContainer = [];
  useEffect(() => {
    toasters.forEach((toaster) => {
      const timeout = setTimeout(() => {
        dispatch(uiActions.removeToaster({ id: toaster.id }));
      }, 3000);
      timeoutContainer.unshift(timeout);
    });

    return () => {
      timeoutContainer.forEach((timeout) => {
        clearTimeout(timeout);
      });
    };
  }, [toasters]);
  return (
    <div className={classes.toaster__container}>
      {toasters.map((toaster) => (
        <div
          key={toaster.id}
          className={`${classes.toaster__content} ${
            toaster.type === "success"
              ? classes.success
              : toaster.type === "error"
              ? classes.error
              : ""
          }`}
        >
          {toaster.message}
        </div>
      ))}
    </div>
  );
};

export default Toaster;
