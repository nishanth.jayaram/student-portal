import React from "react";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import "./App.css";
import Layout from "./layouts/Layout";
import Home from "./pages/home";
import Loader from "./ui/Loader/Loader";
import { useSelector } from "react-redux";
import Toaster from "./ui/Toaster/Toaster";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [{ index: true, element: <Home /> }],
  },
]);

function App() {
  const showLoader = useSelector((state) => state?.ui?.showLoader);
  return (
    <>
      <RouterProvider router={router}></RouterProvider>
      {showLoader && <Loader />}
      <Toaster />
    </>
  );
}

export default App;
