import React from "react";

const Input = ({ input, label, hasError, errorMsg }) => {
  return (
    <div className="input__container">
      <label htmlFor={input.id}>{label}</label>
      <input {...input} />
      {hasError && <p className="error__msg">{errorMsg}</p>}
    </div>
  );
};

export default Input;
