import React from "react";

const Select = ({ options, input, label, hasError, errorMsg }) => {
  return (
    <div className="input__container">
      <label htmlFor={input.id}>{label}</label>
      <select {...input}>
        {input.placeholder && (
          <option value="" disabled>
            {input.placeholder}
          </option>
        )}
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.name}
          </option>
        ))}
      </select>
      {hasError && <p className="error__msg">{errorMsg}</p>}
    </div>
  );
};

export default Select;
