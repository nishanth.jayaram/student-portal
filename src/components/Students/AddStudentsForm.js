import { useDispatch, useSelector } from "react-redux";
import useInput from "../../hooks/use-input";
import Input from "../Input/Input";
import Select from "../Select/Select";
import classes from "./AddStudentsForm.module.css";
import { uiActions } from "../../store/ui";
import { studentActions } from "../../store/students";
import Textarea from "../Textarea/Textarea";
import { useCallback, useEffect } from "react";
import axiosInstance from "../../services/axiosInstance";

const ageValidation = (value) => value !== "0";
const classValidation = (value) => Number(value) <= 12 && value !== "0";

const genderOptions = [
  { name: "Male", value: "M" },
  { name: "Female", value: "F" },
  { name: "Others", value: "O" },
];

const AddStudentsForm = ({ studentDtl = {}, onClose }) => {
  const dispatch = useDispatch();
  const studentsData = useSelector((state) => state?.student?.data);

  useEffect(() => {
    if (studentDtl._id) {
      firstNameChangeHandler({ target: { value: studentDtl.first_name } });
      lastNameChangeHandler({ target: { value: studentDtl.last_name } });
      ageChangeHandler({ target: { value: studentDtl.age } });
      genderChangeHandler({ target: { value: studentDtl.gender } });
      classChangeHandler({ target: { value: studentDtl.class } });
      addressChangeHandler({ target: { value: studentDtl.address } });
    }
  }, []);

  const {
    value: firstNameValue,
    isValid: firstNameIsValid,
    hasError: firstNameHasError,
    valueChangeHandler: firstNameChangeHandler,
    inputBlurHandler: firstNameBlurHandler,
    reset: resetFirstName,
  } = useInput({ isAlphaOnly: true });

  const {
    value: lastNameValue,
    isValid: lastNameIsValid,
    hasError: lastNameHasError,
    valueChangeHandler: lastNameChangeHandler,
    inputBlurHandler: lastNameBlurHandler,
    reset: resetLastName,
  } = useInput({ isAlphaOnly: true });

  const {
    value: ageValue,
    isValid: ageIsValid,
    hasError: ageHasError,
    hasCustomVldError: ageHasCustomError,
    valueChangeHandler: ageChangeHandler,
    inputBlurHandler: ageBlurHandler,
    reset: resetAge,
  } = useInput({ isNumOnly: true, customValidation: ageValidation });

  const {
    value: genderValue,
    isValid: genderIsValid,
    hasError: genderHasError,
    valueChangeHandler: genderChangeHandler,
    inputBlurHandler: genderBlurHandler,
    reset: resetGender,
  } = useInput();

  const {
    value: classValue,
    isValid: classIsValid,
    hasError: classHasError,
    hasCustomVldError: classHasCustomError,
    valueChangeHandler: classChangeHandler,
    inputBlurHandler: classBlurHandler,
    reset: resetClass,
  } = useInput({ isNumOnly: true, customValidation: classValidation });

  const {
    value: addressValue,
    isValid: addressIsValid,
    hasError: addressHasError,
    valueChangeHandler: addressChangeHandler,
    inputBlurHandler: addressBlurHandler,
    reset: resetAddress,
  } = useInput();

  let formIsValid = false;

  if (
    firstNameIsValid &&
    lastNameIsValid &&
    ageIsValid &&
    genderIsValid &&
    classIsValid &&
    addressIsValid
  ) {
    formIsValid = true;
  }

  const generateNewID = useCallback(() => {
    return studentsData.length
      ? Math.max(...studentsData.map((eachData) => eachData.id))
      : 0;
  }, [studentsData]);

  const submitHandler = async (event) => {
    try {
      dispatch(uiActions.showHideLoader(true));
      event.preventDefault();

      if (!formIsValid) {
        return;
      }

      const actionMethod = studentDtl._id ? "edit" : "add";
      const payload = {
        first_name: firstNameValue,
        last_name: lastNameValue,
        age: ageValue,
        gender: genderValue,
        class: classValue,
        address: addressValue,
      };
      let response;
      if (actionMethod === "edit") {
        response = await axiosInstance.put(
          `/student/${studentDtl._id}`,
          payload
        );
      } else {
        payload.id = generateNewID() + 1;
        response = await axiosInstance.post("/student", payload);
      }

      dispatch(studentActions[actionMethod](response.data));

      resetFirstName();
      resetLastName();
      resetAge();
      resetGender();
      resetClass();
      resetAddress();
      onClose();
      dispatch(uiActions.showHideLoader(false));
      dispatch(
        uiActions.addToaster({
          type: "success",
          message: `${payload.first_name} ${payload.last_name} ${actionMethod}ed successfully!`,
        })
      );
    } catch (error) {
      dispatch(uiActions.showHideLoader(false));
      dispatch(
        uiActions.addToaster({
          type: "error",
          message: error?.message || "Something went wrong",
        })
      );
    }
  };

  return (
    <form onSubmit={submitHandler}>
      <h3 className={classes.header}>
        {studentDtl._id ? "Edit" : "Add"} Student
      </h3>
      <div className={classes.add__student__container}>
        <Input
          label="First Name *"
          hasError={firstNameHasError}
          errorMsg="Please enter first name."
          input={{
            id: "first_name",
            type: "text",
            minLength: "2",
            maxLength: "50",
            value: firstNameValue,
            onChange: firstNameChangeHandler,
            onBlur: firstNameBlurHandler,
          }}
        />
        <Input
          label="Last Name *"
          hasError={lastNameHasError}
          errorMsg="Please enter last name."
          input={{
            id: "last_name",
            type: "text",
            maxLength: "50",
            value: lastNameValue,
            onChange: lastNameChangeHandler,
            onBlur: lastNameBlurHandler,
          }}
        />
        <Input
          label="Age *"
          hasError={ageHasError}
          errorMsg={
            ageHasCustomError ? "Please enter valid age." : "Please enter age."
          }
          input={{
            id: "age",
            type: "text",
            value: ageValue,
            maxLength: "2",
            onChange: ageChangeHandler,
            onBlur: ageBlurHandler,
          }}
        />
        <Select
          label="Gender *"
          options={genderOptions}
          hasError={genderHasError}
          errorMsg="Please select gender."
          input={{
            id: "gender",
            type: "text",
            value: genderValue,
            onBlur: genderBlurHandler,
            onChange: genderChangeHandler,
            placeholder: "Select a gender",
          }}
        />
        <Input
          label="Class *"
          hasError={classHasError}
          errorMsg={
            classHasCustomError
              ? "Please enter valid class."
              : "Please enter class."
          }
          input={{
            id: "class",
            type: "text",
            maxLength: "2",
            value: classValue,
            onChange: classChangeHandler,
            onBlur: classBlurHandler,
          }}
        />
        <Textarea
          label="Address *"
          hasError={addressHasError}
          errorMsg="Please enter address."
          input={{
            id: "address",
            rows: "4",
            maxLength: "160",
            value: addressValue,
            onChange: addressChangeHandler,
            onBlur: addressBlurHandler,
          }}
        />
      </div>
      <div className={classes.btn__container}>
        <button
          type="button"
          className={classes.btn__close}
          onClick={() => onClose()}
        >
          Close
        </button>
        <button disabled={!formIsValid} className={classes.btn__submit}>
          Submit
        </button>
      </div>
    </form>
  );
};

export default AddStudentsForm;
