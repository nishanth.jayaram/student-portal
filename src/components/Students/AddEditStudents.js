import React from "react";
import Modal from "../../ui/Modal/Modal";
import AddStudentsForm from "./AddStudentsForm";

const AddEditStudents = ({ showModal, showHideModal, studentDtl }) => {
  return (
    <Modal isOpen={showModal}>
      <AddStudentsForm onClose={showHideModal} studentDtl={studentDtl} />
    </Modal>
  );
};

export default AddEditStudents;
