import React from "react";

const Textarea = ({ input, label, hasError, errorMsg }) => {
  return (
    <div className="input__container">
      <label htmlFor={input.id}>{label}</label>
      <textarea {...input}></textarea>
      {hasError && <p className="error__msg">{errorMsg}</p>}
    </div>
  );
};

export default Textarea;
