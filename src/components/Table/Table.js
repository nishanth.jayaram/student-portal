import React, { useEffect } from "react";
import classes from "./Table.module.css";
import Pagination from "../Pagination/Pagination";
import usePagination from "../../hooks/use-pagination";
import useSortableTable from "../../hooks/use-sort";

const formatHeading = (heading) => {
  return heading
    .split("_")
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
    .join(" ");
};

const Table = ({
  tableData,
  tableHeaders,
  onEdit,
  onDelete,
  itemsPerPage = 5,
  defaultSortBy = "id",
}) => {
  const { totalPages, currentPage, pageChangeHandler, visiblePageNumbers } =
    usePagination(tableData.length, itemsPerPage);

  const headers = tableHeaders || Object.keys(tableData[0] || {}) || [];

  const { sortedData, sortBy, sortDirection, toggleSort, sortData } =
    useSortableTable(tableData, defaultSortBy);

  useEffect(() => {
    sortData(tableData);
  }, [tableData]);

  const startIndex = (currentPage - 1) * Number(itemsPerPage);
  const visibleData = sortedData.slice(
    startIndex,
    startIndex + Number(itemsPerPage)
  );

  return (
    <>
      {tableData?.length > 0 && (
        <>
          <table className={classes.table__container}>
            <thead>
              <tr>
                {headers.map((eachHeader, index) => (
                  <th
                    className={classes.table__header}
                    key={index}
                    onClick={() => {
                      toggleSort(eachHeader);
                    }}
                  >
                    {formatHeading(eachHeader)}
                    {sortBy === eachHeader && (
                      <span className={classes.table__sort}>
                        {sortDirection === "asc" ? "↑" : "↓"}
                      </span>
                    )}
                  </th>
                ))}
                <th className={classes.table__action}>Action</th>
              </tr>
            </thead>
            <tbody>
              {visibleData.map((eachData, dIndex) => (
                <tr key={dIndex}>
                  {headers.map((eachHeader, hIndex) => (
                    <td
                      title={
                        eachData[eachHeader].length > 54
                          ? eachData[eachHeader]
                          : null
                      }
                      className={`${classes.table__values} ${classes.table__text__ellipsis}`}
                      key={hIndex}
                    >
                      {eachData[eachHeader] || "-"}
                    </td>
                  ))}
                  <td className={classes.table__values}>
                    <button
                      className={classes.table__action__edit}
                      onClick={() => onEdit(eachData)}
                    >
                      Edit
                    </button>
                    <button
                      className={classes.table__action__del}
                      onClick={() => onDelete(eachData)}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <Pagination
            totalPages={totalPages}
            currentPage={currentPage}
            onPageChange={pageChangeHandler}
            visiblePageNumbers={visiblePageNumbers}
          />
        </>
      )}
      {tableData?.length === 0 && (
        <p className={classes.empty__data}>No records found.</p>
      )}
    </>
  );
};

export default Table;
