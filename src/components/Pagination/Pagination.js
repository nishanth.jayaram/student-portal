import React from "react";
import classes from "./Pagination.module.css";

const Pagination = ({
  totalPages,
  currentPage,
  onPageChange,
  visiblePageNumbers,
}) => {
  return (
    <div className={classes.pagination}>
      {currentPage > 1 && (
        <button
          onClick={() => onPageChange(1)}
          className={classes.pagination__button}
        >
          &lt;&lt;
        </button>
      )}
      {visiblePageNumbers().map((page) => (
        <button
          key={page}
          onClick={() => onPageChange(page)}
          className={`${classes.pagination__button} ${
            currentPage === page ? classes.active : ""
          }`}
        >
          {page}
        </button>
      ))}
      {currentPage < totalPages && (
        <button
          onClick={() => onPageChange(totalPages)}
          className={classes.pagination__button}
        >
          &gt;&gt;
        </button>
      )}
      <span className={classes.pagination__info}>
        {currentPage} of {totalPages} pages
      </span>
    </div>
  );
};

export default Pagination;
