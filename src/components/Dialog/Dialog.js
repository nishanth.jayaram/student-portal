import React from "react";
import Modal from "../../ui/Modal/Modal";
import classes from "./Dialog.module.css";

const Dialog = ({
  showModal,
  header,
  message,
  isConfirmation,
  onClose,
  onConfirm,
}) => {
  return (
    <Modal isOpen={showModal}>
      <div className={classes.dialog__container}>
        <h3 className={classes.header}>{header}</h3>
        <p className={classes.message}>{message}</p>
        <div className={classes.btn__container}>
          <button onClick={() => onClose()}>
            {isConfirmation ? "No" : "Ok"}
          </button>
          {isConfirmation && <button onClick={() => onConfirm()}>Yes</button>}
        </div>
      </div>
    </Modal>
  );
};

export default Dialog;
