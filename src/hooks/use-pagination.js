import { useState, useCallback } from "react";

const usePagination = (totalItems, itemsPerPage) => {
  const [currentPage, setCurrentPage] = useState(1);
  const totalPages = Math.ceil(totalItems / itemsPerPage);

  const pageChangeHandler = useCallback((page) => {
    setCurrentPage(page);
  }, []);

  const renderPageNumbers = useCallback(() => {
    const pageNumbers = [];
    for (let i = 1; i <= totalPages; i++) {
      pageNumbers.push(i);
    }
    return pageNumbers;
  }, [totalPages]);

  const visiblePageNumbers = useCallback(() => {
    const allPageNumbers = renderPageNumbers();
    const maxVisiblePages = 3;

    if (allPageNumbers.length <= maxVisiblePages) {
      return allPageNumbers;
    }

    const firstVisiblePage = Math.max(
      currentPage - Math.floor(maxVisiblePages / 2),
      1
    );
    const lastVisiblePage = Math.min(
      firstVisiblePage + maxVisiblePages - 1,
      totalPages
    );

    return allPageNumbers.slice(firstVisiblePage - 1, lastVisiblePage);
  }, [currentPage, totalPages]);

  return {
    totalPages,
    currentPage,
    pageChangeHandler,
    visiblePageNumbers,
  };
};

export default usePagination;
