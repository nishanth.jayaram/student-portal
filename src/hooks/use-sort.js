import { useReducer, useCallback, useEffect } from "react";

const initialSortState = {
  data: [],
  sortBy: "",
  sortDirection: "desc",
};

const sortReducer = (state, action) => {
  if (action.type === "SET_DATA") {
    return { ...state, data: action.payload };
  }
  if (action.type === "TOGGLE_SORT") {
    if (action.payload === state.sortBy) {
      return {
        ...state,
        sortDirection: state.sortDirection === "asc" ? "desc" : "asc",
      };
    } else {
      return {
        ...state,
        sortBy: action.payload,
        sortDirection: "asc",
      };
    }
  }
  return state;
};

const useSortableTable = (initialData, initialSortBy) => {
  const [sortState, dispatch] = useReducer(sortReducer, {
    ...initialSortState,
    data: initialData,
    sortBy: initialSortBy,
  });

  useEffect(() => {
    sortData(sortState.data);
  }, [sortState.sortBy, sortState.sortDirection]);

  const sortData = useCallback(
    (newData) => {
      const sortedData =
        [...newData]?.sort((a, b) => {
          const aValue = Number(a[sortState.sortBy])
            ? Number(a[sortState.sortBy])
            : a[sortState.sortBy];
          const bValue = Number(b[sortState.sortBy])
            ? Number(b[sortState.sortBy])
            : b[sortState.sortBy];
          if (aValue < bValue)
            return sortState.sortDirection === "asc" ? -1 : 1;
          if (aValue > bValue)
            return sortState.sortDirection === "asc" ? 1 : -1;
          return 0;
        }) || [];
      dispatch({ type: "SET_DATA", payload: sortedData });
    },
    [sortState]
  );

  const toggleSort = useCallback((newSortBy) => {
    dispatch({ type: "TOGGLE_SORT", payload: newSortBy });
  }, []);

  return {
    sortedData: sortState.data,
    sortBy: sortState.sortBy,
    sortDirection: sortState.sortDirection,
    toggleSort,
    sortData,
  };
};

export default useSortableTable;
