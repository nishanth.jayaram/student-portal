import { useCallback, useReducer } from "react";

const initialInputState = {
  value: "",
  isTouched: false,
};

const inputStateReducer = (state, action) => {
  if (action.type === "INPUT") {
    return { value: action.value, isTouched: state.isTouched };
  }
  if (action.type === "BLUR") {
    return { isTouched: true, value: state.value };
  }
  if (action.type === "RESET") {
    return { isTouched: false, value: "" };
  }
  return state;
};

const isNotEmpty = (value) => value.trim() !== "";

const useInput = ({
  isNumOnly = false,
  isAlphaOnly = false,
  customValidation = (value) => true,
} = {}) => {
  const [inputState, dispatch] = useReducer(
    inputStateReducer,
    initialInputState
  );

  const valueIsValid =
    isNotEmpty(inputState.value) && customValidation(inputState.value);
  const hasError = !valueIsValid && inputState.isTouched;
  const hasCustomVldError =
    !customValidation(inputState.value) && inputState.isTouched;

  const checkAllowedKeys = useCallback(
    (value) => {
      const regex = isAlphaOnly ? /^[a-zA-Z]+$/ : isNumOnly ? /^\d*$/ : null;
      return regex?.test(value);
    },
    [isAlphaOnly, isNumOnly]
  );

  const valueChangeHandler = useCallback(
    (event) => {
      const value = event?.target?.value || "";
      if (value && (isAlphaOnly || isNumOnly) && !checkAllowedKeys(value)) {
        return;
      }
      dispatch({
        type: "INPUT",
        value,
      });
    },
    [isAlphaOnly, isNumOnly]
  );

  const inputBlurHandler = useCallback((event) => {
    dispatch({ type: "BLUR" });
  }, []);

  const reset = useCallback(() => {
    dispatch({ type: "RESET" });
  }, []);

  return {
    value: inputState.value,
    isValid: valueIsValid,
    hasError,
    hasCustomVldError,
    valueChangeHandler,
    inputBlurHandler,
    reset,
  };
};

export default useInput;
